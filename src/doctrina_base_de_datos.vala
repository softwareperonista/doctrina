/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Sqlite;
using Doctrina;

public class Doctrina.BaseDeDatos : Object {
  private Database db;
  private string filename;
  private InfraestructuraInterface infra;

  public BaseDeDatos (string filename, InfraestructuraInterface infra) {
    this.filename = filename;
    this.infra = infra;
    this.inicializar ();
  }

  private void inicializar () {
    this.db = open ();
    if (this.db != null) {
      if (this.db.exec ("SELECT * FROM db", null, null) != Sqlite.OK) {
        this.crear_tablas ();
      } else {
        this.comprobar_version ();
      }
    }
  }

  private Database open () {
    Database db;
    int result_code;

    var archivo_db = File.new_for_path (this.archivo_db ());

    if (!archivo_db.query_exists ()) {
      this.crear_archivo_db ();
    }

    while (Database.open (this.archivo_db (), out db) != Sqlite.OK) {
      stderr.printf (("Could not open the data base") + ": %s\n", db.errmsg ());
      if (!this.crear_archivo_db ()) {
        break;
      }
    }

    if (db != null) {
      result_code = db.exec ("PRAGMA foreign_keys=ON;", null, null);

      if (result_code != Sqlite.OK) {
        stderr.printf ("SQL error: %d, %s\n", result_code, db.errmsg ());
      }
    }

    return db;
  }

  private bool crear_archivo_db () {
    bool retorno = true;

    if (retorno != false) {
      var archivo_db = File.new_for_path (this.archivo_db ());
      try {
        archivo_db.create (FileCreateFlags.PRIVATE);
      } catch (Error e) {
        stderr.printf ("Error: %s\n", e.message);
        retorno = false;
      }
    }

    return retorno;
  }

  private bool crear_tablas () {
    bool retorno = true;
    string definicion_db;
    string mensaje_error = "";

    definicion_db = this.infra.definicion_db ();

    debug ("inicializando talbas de la db");
    var result_code = db.exec (definicion_db, null, out mensaje_error);

    if (result_code != Sqlite.OK) {
      retorno = false;
      debug ("Error al cargar la definición de la db: %s", mensaje_error);
    }

    this.insert ("db", "version", this.infra.version_db ());
    return retorno;
  }

  private void comprobar_version () {
    var version = this.select ("db", "version", "").index (0);
    debug ("version del archivo de la db: " + version);
    debug ("version del archivo de la infraestructura: " + this.infra.version_db ());

    if (version != this.infra.version_db ()) {
      debug ("la version del archivo de la db y de la infrastrucrura difieren");
      if (this.backup_db (this.infra.version_db ())) {
        this.inicializar ();
      }
    }
  }

  private bool backup_db (string version) {
    bool retorno = true;
    var archivo_db = File.new_for_path (this.archivo_db ());
    var archivo_backup = File.new_for_path (this.archivo_db () + "." + version + ".bak");
    debug ("creando backup del archivo de la db");
    try {
      archivo_db.move (archivo_backup, FileCopyFlags.NONE, null, null);
    } catch (Error e) {
      stderr.printf ("%s\n", e.message);
      retorno = false;
    }

    return retorno;
  }

  private ResultadoQuery query (string sql_query, Array<string> values) {
    Statement stmt;

    this.debug_con_valores (sql_query, values);
    var result_code = db.prepare_v2 (sql_query, -1, out stmt);

    if (result_code != Sqlite.OK) {
      stderr.printf (sql_query);
      stderr.printf (("Failed to execute the sentence\n") + ": %s - %d - %s", sql_query, result_code, db.errmsg ());
      return new ResultadoQuery.error ();
    }

    for (int i = 0; i < values.length; i++) {
      stmt.bind_text (i + 1, values.index (i));
    }

    var rows = this.parse_query (stmt);
    return new ResultadoQuery (true, rows);
  }

  private void debug_con_valores (string sql_query, Array<string> values) {
    var lista_de_values = "";
    if (values.length > 0) {
      lista_de_values = " -- (%s)".printf (Utiles.join_array (values.data, ", ", "'"));
    }
    debug ("ejecutando query: %s%s", sql_query, lista_de_values);
  }

  private ResultadoQuery query_where (string sql_query, string where) {
    var valores = this.parsear_where (where);
    var where_parametrizado = this.parametrizar_where (where);

    return this.query (sql_query + " " + where_parametrizado, valores);
  }

  public bool insert (string tabla, string columnas, string valores) {

    var valores_arr = this.parsear_valores_insert (valores);

    var parametros = this.crear_parametros_query (valores_arr.length);
    string query = "INSERT INTO \"" + tabla + "\" (" + columnas + ") VALUES (" + parametros + ")";
    var resultado = this.query (query, valores_arr);

    return resultado.resultado;
  }

  public bool del (string tabla, string where = "") {
    var resultado = this.query_where ("DELETE FROM " + tabla, where);

    return resultado.resultado;
  }

  public bool update (string tabla, Array<string> valores, string condicion) {
    string datos = "";

    var where = "WHERE " + condicion;

    for ( int i = 0; i < valores.length; i++ ) {
      datos += valores.index (i);

      if (i != (valores.length - 1)) {
        datos += ", ";
      }
    }

    string query = "UPDATE " + tabla + " SET " + datos + " " + where;

    var resultado = this.query (query, new Array<string> ());

    return resultado.resultado;
  }

  public Array<string> select (string tabla, string columnas, string where = "") {
    var resultado = this.query_where ("SELECT " + columnas + " FROM " + tabla, where);

    return resultado.rows;
  }

  public Array<string> select_distinct (string tabla, string columnas, string where = "") {
    var resultado = this.query_where ("SELECT DISTINCT " + columnas + " FROM " + tabla, where);

    return resultado.rows;
  }

  public Array<string> count (string tabla, string where) {
    var resultado = this.query_where ("SELECT COUNT (*) FROM " + tabla, where);

    return resultado.rows;
  }

  private Array<string> parse_query (Statement stmt) {
    Array<string> tuplas = new Array<string> ();
    string tupla;
    string campo;

    int cols = stmt.column_count ();
    int rc = stmt.step ();

    while (rc == Sqlite.ROW) {
      switch (rc) {
      case Sqlite.DONE:
        break;
      case Sqlite.ROW:
        tupla = "";
        for ( int i = 0; i < cols; i++ ) {
          campo = stmt.column_text (i);
          tupla += campo;
          if (i < cols - 1) {
            tupla += "|";
          }
        }
        tuplas.append_val (tupla);
        break;
      default:
        print ("Error parseando la query");
        break;
      }

      rc = stmt.step ();
    }

    return tuplas;
  }

  public string archivo_db () {
    return this.infra.directorio_de_configuracion () + "/" + this.filename;
  }

  public void begin_transaction () {
    this.query ("BEGIN TRANSACTION;", new Array<string> ());
  }

  public void commit_transaction () {
    this.query ("COMMIT;", new Array<string> ());
  }

  public void rollback_transaction () {
    this.query ("ROLLBACK;", new Array<string> ());
  }

  public string ultimo_id (string tabla) {
    return this.select ("sqlite_sequence", "seq", "WHERE name='" + tabla + "'").index (0);
  }

  private Array<string> parsear_valores_insert (string valores) {
    var retorno = new Array<string> ();
    var valores_separados = valores.split (",", -1);

    for (int i = 0; i < valores_separados.length; i++) {
      if (valores_separados[i].contains (";")) {
        break;
      }

      var valor = valores_separados[i];
      if (valores_separados[i][0] == '\'' || valores_separados[i][0] == '"') {
        var caracter_de_cadena = valores_separados[i][0];
        for (int j = i; j < valores_separados.length; j++) {
          if (valores_separados[j][valores_separados[j].length - 1] == caracter_de_cadena) {
            valor = Doctrina.Utiles.join_array (valores_separados[i: j + 1], ",");
            i += (j - i);
            j = valores_separados.length + 1;
          }
        }
      }

      valor = valor.replace ("\"", "").replace ("'", "").chug ().chomp ();
      retorno.append_val (valor);
    }

    return retorno;
  }

  private string crear_parametros_query (uint cantidad) {
    var retorno = "";

    for (uint i = 0; i < cantidad; i++) {
      if (retorno != "") {
        retorno = retorno + ", ";
      }
      retorno = retorno + "$%u".printf (i);
    }

    return retorno;
  }

  protected Array<string> parsear_where (string where) {
    var retorno = new Array<string> ();
    var where_separados = where.split ("=", -1);

    for (int i = 1; i < where_separados.length; i++) {
      var separado = where_separados[i].chug ().chomp ().split (" ", -1);
      if (separado.length == 0) {
        continue;
      }

      var valor = separado[0];
      if (separado[0][0] == '\'' || separado[0][0] == '"') {
        var caracter_de_cadena = separado[0][0];
        for (int j = 1; j < separado.length; j++) {
          if (separado[j][separado[j].length - 1] == caracter_de_cadena) {
            valor = Doctrina.Utiles.join_array (separado[0 : j + 1], " ");
          }
        }
      } else {
        if (separado[0].contains (".")) {
          continue;
        }
      }

      valor = valor.replace ("\"", "").replace ("'", "").chug ().chomp ();
      if (valor.contains (";")) {
        break;
      }
      retorno.append_val (valor);
    }

    return retorno;
  }

  protected string parametrizar_where (string where) {
    var where_separados = where.split ("=", -1);

    if (where_separados.length == 0) {
      return where;
    }

    var retorno = where_separados[0].chug ().chomp ();
    for (int i = 1; i < where_separados.length; i++) {
      var separado = where_separados[i].chug ().chomp ().split (" ", -1);
      var primero = 1;
      var parametro = " $%d".printf (i);
      if (separado[0][0] == '\'' || separado[0][0] == '"') {
        var caracter_de_cadena = separado[0][0];
        for (int j = 1; j < separado.length; j++) {
          if (separado[j][separado[j].length - 1] == caracter_de_cadena) {
            primero = j + 1;
          }
        }
      } else {
        if (separado[0].contains (".")) {
          primero = 0;
          parametro = "";
        }
      }
      retorno = "%s =%s %s".printf (retorno, parametro, Doctrina.Utiles.join_array (separado[primero:], " "));
    }

    return retorno;
  }
}
