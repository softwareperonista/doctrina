/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

public class Doctrina.Infraestructura : Object, Doctrina.InfraestructuraInterface {
  private string nombre;
  private string version;
  private string db_resource;

  public Infraestructura (string nombre, string version, string db_resource) {
    this.nombre = nombre;
    this.version = version;
    this.db_resource = db_resource;
  }

  public string directorio_de_configuracion () {
    var config_dir = GLib.Environment.get_user_config_dir () + "/" + this.nombre;

    if (!(Utiles.existe_path (config_dir))) {
      Utiles.crear_directorio (config_dir);
    }

    return config_dir;
  }

  public string version_db () {
    return this.version;
  }

  public string definicion_db () {
    string definicion_db = "";
    uint8[] contenido;
    string etag_out;


    var file_definicion_db = File.new_for_uri (this.db_resource);

    try {
      file_definicion_db.load_contents (null, out contenido, out etag_out);
    } catch (GLib.Error e) {
      stderr.printf ("%s\n", e.message);
    }

    definicion_db = (string) contenido;

    return definicion_db;
  }

  [Version (deprecated = true, deprecated_since = "0.1.14.0", replacement = "Doctrina.Utiles.existe_path")]
  public static bool existe_path (string path) {
    return Utiles.existe_path (path);
  }

  [Version (deprecated = true, deprecated_since = "0.1.14.0", replacement = "Doctrina.Utiles.crear_directorio")]
  public static void crear_directorio (string path) {
    Utiles.crear_directorio (path);
  }
}
