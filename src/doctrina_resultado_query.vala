/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

public class ResultadoQuery : Object {
  public bool resultado { get; private set; }
  public Array<string> rows { get; private set; }

  public ResultadoQuery (bool resultado, Array<string> rows) {
    this.resultado = resultado;
    this.rows = rows;
  }

  public ResultadoQuery.error () {
    this.resultado = false;
    this.rows = new Array<string> ();
  }
}
