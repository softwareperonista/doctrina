/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

public class Doctrina.Utiles : Object {
  public static string separador_de_miles (string cadena) {
    int numero = int.parse (cadena);

    string retorno = cadena;
    int numero_temp = numero;
    int cant_puntos = 0;
    int[] separados = new int[10];

    if (numero != 0) {

      retorno = "";
      numero_temp /= 1000;

      while (numero_temp > 0) {
        cant_puntos++;
        numero_temp /= 1000;
      }

      for ( int i = 0; i <= cant_puntos; i++ ) {
        int resto = numero % 1000;
        numero /= 1000;

        separados[(cant_puntos - i)] = resto;
      }

      for ( int i = 0; i <= cant_puntos; i++ ) {
        if ((separados[i] < 100) && (i != 0)) {
          retorno += "0";
          if (separados[i] < 10) {
            retorno += "0";
          }
        }

        retorno += separados[i].to_string ();

        if (i != cant_puntos) {
          retorno += ".";
        }
      }
    }
    return retorno;
  }

  public static string parse_cuil (string cuil) {
    string retorno = cuil;

    if (cuil.length == 11) {
      retorno = cuil[0 : 2];
      retorno += "-";
      retorno += cuil[2 : 10];
      retorno += "-";
      retorno += cuil[10 : 11];
    }
    return retorno;
  }

  public static string parse_fecha (string cadena, string formato = "%e de %B de %Y") {
    string retorno = cadena;

    if (cadena.length == 8) {
      var fecha = new GLib.DateTime (new GLib.TimeZone.local (),
                                     int.parse (cadena[0 : 4]),
                                     int.parse (cadena[4 : 6]),
                                     int.parse (cadena[6 : 8]),
                                     0, 0, 0);

      if (fecha != null) {
        retorno = fecha.format (formato).chug ();
      }
    }

    return retorno;
  }

  public static string convertir_a_dos_digitos (string texto) {
    string retorno = "";

    if (texto != "") {
      int numero = int.parse (texto);

      if (numero < 10) {
        retorno = "0";
      }

      retorno += numero.to_string ();
    }


    return retorno;
  }

  public static string parse_telefono (string? telefono) {
    string retorno = "";

    if (telefono != null) {
      retorno = telefono;
      if (telefono.length == 13) {
        retorno = telefono[3 : 5];
        retorno += " ";
        retorno += telefono[5 : 9];
        retorno += " ";
        retorno += telefono[9 : 13];
      } else if (telefono.length == 12) {
        retorno = telefono[2 : 4];
        retorno += " ";
        retorno += telefono[4 : 8];
        retorno += " ";
        retorno += telefono[8 : 12];
      } else if (telefono.length == 11) {
        retorno = telefono[0 : 3];
        retorno += " ";
        retorno += telefono[3 : 7];
        retorno += " ";
        retorno += telefono[7 : 11];
      } else if (telefono.length == 10) {
        retorno = telefono[0 : 2];
        retorno += " ";
        retorno += telefono[2 : 6];
        retorno += " ";
        retorno += telefono[6 : 10];
      } else if (telefono.length == 8) {
        retorno = telefono[0 : 4];
        retorno += " ";
        retorno += telefono[4 : 8];
      }
    }

    return retorno;
  }

  public static int mm_to_px (int mm, int dpi) {
    int retorno = 0;

    double en_pulgadas = mm / 25.4;
    double en_pixeles = en_pulgadas * dpi;
    retorno = (int) en_pixeles;

    return retorno;
  }

  public static string campo_a_nombre (string campo) {
    string retorno = campo;

    retorno = retorno.replace ("_", " ");
    retorno = retorno.replace ("-", " ");

    var palabras = retorno.split (" ");

    retorno = "";
    for ( int i = 0; i < palabras.length; i++ ) {
      var primera_letra = palabras[i].substring (0, 1);
      var resto_de_la_palabra = palabras[i].substring (1);

      var palabra = primera_letra.up () + resto_de_la_palabra.down ();

      retorno += palabra + " ";
    }

    return retorno;
  }

  public static void crear_directorio (string path) {
    var directorio = File.new_for_path (path);
    try {
      directorio.make_directory_with_parents ();
    } catch (Error e) {
      error (e.message);
    }
  }

  public static bool existe_path (string path) {
    var archivo = File.new_for_path (path);
    var retorno = archivo.query_exists ();

    return retorno;
  }

  public static string join_array (string[] array, string separador = "", string caracter_de_cadena = "") {
    var retorno = "";
    for (int i = 0; i < array.length; i++ ) {
      retorno += "%s%s%s".printf (caracter_de_cadena, array[i], caracter_de_cadena);
      if (i < (array.length) -1) {
        retorno += separador;
      }
    }

    return retorno.chug ().chomp ();
  }
}
