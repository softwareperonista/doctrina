/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* doctrina_base_de_datos_parsear_where.vala
 *
 * Copyright 2024 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class DoctrinaBaseDeDatosParsearWhere : Doctrina.BaseDeDatos {
  public DoctrinaBaseDeDatosParsearWhere (string filename, Doctrina.InfraestructuraInterface infra) {
    base (filename, infra);
  }

  public new Array<string> parsear_where (string where) {
    return base.parsear_where (where);
  }

  public new string parametrizar_where (string where) {
    return base.parametrizar_where (where);
  }
}
