/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* doctrina_base_de_datos_test.vala
 *
 * Copyright 2023-2024 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class DoctrinaBaseDeDatosTest {
  public static int main (string[] args) {
    Test.init (ref args);

    DoctrinaBaseDeDatosTest.agregar_tests ();

    Test.run ();

    return 0;
  }

  public static void agregar_tests () {
    DoctrinaBaseDeDatosTest.agregar_insert_tests ();
  }

  public static void agregar_insert_tests () {
    Test.add_func ("/insert/ok", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "\"texto\", 2";
      var resultado_esperado = true;

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("insert-ok-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);

      assert (resultado_insert == resultado_esperado);

      var resultado_select = db.select ("tabla", "columna1", "");

      assert (resultado_select.length == 1);

      if (resultado_select.length == 1) {
        assert (resultado_select.index (0) == "texto");
      }
    });

    Test.add_func ("/insert/missing-value", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "\"texto\"";
      var resultado_esperado = false;

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("insert-missing-value-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);

      assert (resultado_insert == resultado_esperado);

      var resultado_select = db.select ("tabla", "columna1", "");

      assert (resultado_select.length == 0);
    });

    Test.add_func ("/insert/more-values", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "\"texto\", 2 , 5";
      var resultado_esperado = false;

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("insert-more-values-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);

      assert (resultado_insert == resultado_esperado);

      var resultado_select = db.select ("tabla", "columna1", "");

      assert (resultado_select.length == 0);
    });

    Test.add_func ("/insert/apostrofe-en-valores", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "'texto', 2";
      var resultado_esperado = true;

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("insert-apostrofe-en-valores-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);

      assert (resultado_insert == resultado_esperado);

      var resultado_select = db.select ("tabla", "columna1", "");

      assert (resultado_select.length == 1);

      if (resultado_select.length == 1) {
        assert (resultado_select.index (0) == "texto");
      }
    });

    Test.add_func ("/insert/espacio-en-valores", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "'texto texto', 2";
      var resultado_esperado = true;

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("insert-espacio-en-valores-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);

      assert (resultado_insert == resultado_esperado);

      var resultado_select = db.select ("tabla", "columna1", "");

      assert (resultado_select.length == 1);

      if (resultado_select.length == 1) {
        assert (resultado_select.index (0) == "texto texto");
      }
    });

    Test.add_func ("/insert/punto-en-valores", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "'texto.texto', 2";
      var resultado_esperado = true;

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("insert-punto-en-valores-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);

      assert (resultado_insert == resultado_esperado);

      var resultado_select = db.select ("tabla", "columna1", "");

      assert (resultado_select.length == 1);

      if (resultado_select.length == 1) {
        assert (resultado_select.index (0) == "texto.texto");
      }
    });

    Test.add_func ("/insert/sin-espacios-entre-valores", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "texto,2";
      var resultado_esperado = true;

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("insert-sin-espacios-entre-valores-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);

      assert (resultado_insert == resultado_esperado);

      var resultado_select = db.select ("tabla", "columna1", "");

      assert (resultado_select.length == 1);

      if (resultado_select.length == 1) {
        assert (resultado_select.index (0) == "texto");
      }
    });

    Test.add_func ("/insert/sin-espacios-entre-valores-apostrofe", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "'texto',2";
      var resultado_esperado = true;

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("insert-sin-espacios-entre-valores-apostrofe-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);

      assert (resultado_insert == resultado_esperado);

      var resultado_select = db.select ("tabla", "columna1", "");

      assert (resultado_select.length == 1);

      if (resultado_select.length == 1) {
        assert (resultado_select.index (0) == "texto");
      }
    });

    Test.add_func ("/insert/sin-espacios-entre-valores-apostrofe-con-espacios", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "'texto texto',2";
      var resultado_esperado = true;

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("insert-sin-espacios-entre-valores-apostrofe-con-espacios-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);

      assert (resultado_insert == resultado_esperado);

      var resultado_select = db.select ("tabla", "columna1", "");

      assert (resultado_select.length == 1);

      if (resultado_select.length == 1) {
        assert (resultado_select.index (0) == "texto texto");
      }
    });

    Test.add_func ("/insert/sin-espacios-entre-valores-apostrofe-con-coma", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "'texto,texto',2";
      var resultado_esperado = true;

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("insert-sin-espacios-entre-valores-apostrofe-con-coma-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);

      assert (resultado_insert == resultado_esperado);

      var resultado_select = db.select ("tabla", "columna1", "");

      assert (resultado_select.length == 1);

      if (resultado_select.length == 1) {
        assert (resultado_select.index (0) == "texto,texto");
      }
    });

    Test.add_func ("/insert/sin-espacios-entre-valores-doble-apostrofe", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "'deuda_inicial','0'";
      var resultado_esperado = true;

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("insert-sin-espacios-entre-valores-doble-apostrofe-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);

      assert (resultado_insert == resultado_esperado);

      var resultado_select = db.select ("tabla", "columna1", "");

      assert (resultado_select.length == 1);

      if (resultado_select.length == 1) {
        assert (resultado_select.index (0) == "deuda_inicial");
      }
    });

    Test.add_func ("/insert/sin-espacios-entre-valores-tres-valores-con-apostrofe", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER, columna3 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2, columna3";
      var valores = "'20240503','0','1'";
      var resultado_esperado = true;

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("insert-sin-espacios-entre-valores-tres-valores-con-apostrofe-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);

      assert (resultado_insert == resultado_esperado);

      var resultado_select = db.select ("tabla", "columna1", "");

      assert (resultado_select.length == 1);

      if (resultado_select.length == 1) {
        assert (resultado_select.index (0) == "20240503");
      }
    });

    Test.add_func ("/insert/sin-espacios-entre-valores-cuatro-valores-con-apostrofe", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER, columna3 INTEGER, columna4 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2, columna3, columna4";
      var valores = "'20240503','0','1','2'";
      var resultado_esperado = true;

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("insert-sin-espacios-entre-valores-cuatro-valores-con-apostrofe-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);

      assert (resultado_insert == resultado_esperado);

      var resultado_select = db.select ("tabla", "columna1", "");

      assert (resultado_select.length == 1);

      if (resultado_select.length == 1) {
        assert (resultado_select.index (0) == "20240503");
      }
    });

    Test.add_func ("/insert/sin-espacios-entre-valores-cinco-valores-con-apostrofe", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER, columna3 INTEGER, columna4 INTEGER, columna5 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2, columna3, columna4, columna5";
      var valores = "'20240503','0','1','2','3'";
      var resultado_esperado = true;

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("insert-sin-espacios-entre-valores-cinco-valores-con-apostrofe-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);

      assert (resultado_insert == resultado_esperado);

      var resultado_select = db.select ("tabla", "columna1", "");

      assert (resultado_select.length == 1);

      if (resultado_select.length == 1) {
        assert (resultado_select.index (0) == "20240503");
      }
    });

    Test.add_func ("/del/ok", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "\"texto\", 2";
      var valor_del = "2";
      var resultado_esperado = true;

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("insert-more-values-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);
      assert_true (resultado_insert);

      var resultado = db.del (tabla, "WHERE columna2 = %s".printf (valor_del));

      assert (resultado == resultado_esperado);

      var resultado_select = db.select ("tabla", "columna1", "");

      assert (resultado_select.length == 0);
    });

    Test.add_func ("/del/inexistent_row", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "\"texto\", 2";
      var valor_del = "1";
      var resultado_esperado = true;

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("insert-more-values-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);
      assert_true (resultado_insert);

      var resultado = db.del (tabla, "WHERE columna2 = %s".printf (valor_del));

      assert (resultado == resultado_esperado);

      var resultado_select = db.select ("tabla", "columna1", "");

      assert (resultado_select.length == 1);
    });

    Test.add_func ("/update/ok", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "\"texto\", 2";
      var valores_update = new Array<string> ();
      var columna1_update = "texto1";
      valores_update.append_val ("columna1 = \"%s\"".printf (columna1_update));
      var resultado_esperado = true;

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("update-ok-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);
      assert_true (resultado_insert);

      var resultado = db.update (tabla, valores_update, "columna2 = 2");

      assert (resultado == resultado_esperado);

      var resultado_select = db.select ("tabla", "columna1", "");

      assert (resultado_select.length == 1);

      assert (resultado_select.index (0) == columna1_update);
    });

    Test.add_func ("/update/inexistent_row", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "\"texto\", 2";
      var valores_update = new Array<string> ();
      var columna1_update = "texto1";
      valores_update.append_val ("columna1 = \"%s\"".printf (columna1_update));
      var resultado_esperado = true;

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("update-inexistent_row-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);
      assert_true (resultado_insert);

      var resultado = db.update (tabla, valores_update, "columna2 = 4");

      assert (resultado == resultado_esperado);

      var resultado_select = db.select ("tabla", "columna1", "");

      assert (resultado_select.length == 1);

      assert (resultado_select.index (0) == "texto");
    });

    Test.add_func ("/select/ok", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "\"texto\", 2";
      var resultado_esperado = new Array<string> ();
      resultado_esperado.append_val ("texto");

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("select-ok-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);
      assert_true (resultado_insert);

      var resultado = db.select ("tabla", "columna1", "");

      assert (DoctrinaBaseDeDatosTest.comparar_arrays (resultado, resultado_esperado));
    });

    Test.add_func ("/select/where-igual-a-campo", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE tabla2 (columna3 TEXT, columna4 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla1 = "tabla";
      var columnas1 = "columna1, columna2";
      var valores1 = "\"texto\", 2";
      var tabla2 = "tabla2";
      var columnas2 = "columna3, columna4";
      var valores2 = "\"texto1\", 2";
      var resultado_esperado = new Array<string> ();
      resultado_esperado.append_val ("texto|texto1");

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("select-where-igual-a-campo-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla1, columnas1, valores1);
      assert_true (resultado_insert);
      resultado_insert = db.insert (tabla2, columnas2, valores2);
      assert_true (resultado_insert);

      var resultado = db.select ("tabla, tabla2", "columna1, columna3", "WHERE tabla.columna1 = \"texto\" AND tabla.columna2 = tabla2.columna4");

      assert (DoctrinaBaseDeDatosTest.comparar_arrays (resultado, resultado_esperado));
    });

    Test.add_func ("/select/where-campo-con-apostrofe", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "\"texto\", 2";
      var resultado_esperado = new Array<string> ();
      resultado_esperado.append_val ("2");

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("select-where-campo-con-apostrofe-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);
      assert_true (resultado_insert);

      var resultado = db.select ("tabla", "columna2", "WHERE columna1 = 'texto'");

      assert (DoctrinaBaseDeDatosTest.comparar_arrays (resultado, resultado_esperado));
    });

    Test.add_func ("/select-distinct/ok", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "\"texto\", 2";
      var valores2 = "\"texto\", 3";
      var valores3 = "\"texto1\", 4";
      var resultado_esperado = new Array<string> ();
      resultado_esperado.append_val ("texto");
      resultado_esperado.append_val ("texto1");

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("select-distinct-ok-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);
      assert_true (resultado_insert);

      resultado_insert = db.insert (tabla, columnas, valores2);
      assert_true (resultado_insert);

      resultado_insert = db.insert (tabla, columnas, valores3);
      assert_true (resultado_insert);

      var resultado = db.select_distinct ("tabla", "columna1", "");

      assert (DoctrinaBaseDeDatosTest.comparar_arrays (resultado, resultado_esperado));
    });

    Test.add_func ("/count/ok", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var tabla = "tabla";
      var columnas = "columna1, columna2";
      var valores = "\"texto\", 2";
      var valores2 = "\"texto\", 3";
      var valores3 = "\"texto1\", 4";
      var resultado_esperado = new Array<string> ();
      resultado_esperado.append_val ("3");

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new Doctrina.BaseDeDatos ("count-ok-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado_insert = db.insert (tabla, columnas, valores);
      assert_true (resultado_insert);

      resultado_insert = db.insert (tabla, columnas, valores2);
      assert_true (resultado_insert);

      resultado_insert = db.insert (tabla, columnas, valores3);
      assert_true (resultado_insert);

      var resultado = db.count ("tabla", "");

      assert (DoctrinaBaseDeDatosTest.comparar_arrays (resultado, resultado_esperado));
    });

    Test.add_func ("/parsear-where/ok", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var resultado_esperado = new Array<string> ();
      resultado_esperado.append_val ("texto");
      resultado_esperado.append_val ("2");

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new DoctrinaBaseDeDatosParsearWhere ("parsear-where-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado = db.parsear_where ("WHERE columna1 = \"texto\" AND columna2 = 2 LIMIT 1");

      assert (comparar_arrays (resultado, resultado_esperado));
    });

    Test.add_func ("/parsear-where/igual-a-campo", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var resultado_esperado = new Array<string> ();
      resultado_esperado.append_val ("texto");

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new DoctrinaBaseDeDatosParsearWhere ("parsear-where-igual-a-campo-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado = db.parsear_where ("WHERE columna1 = \"texto\" AND columna2 = tabla2.columna LIMIT 1");

      assert (comparar_arrays (resultado, resultado_esperado));
    });

    Test.add_func ("/parsear-where/valor-con-espacio", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var resultado_esperado = new Array<string> ();
      resultado_esperado.append_val ("texto texto");
      resultado_esperado.append_val ("2");

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new DoctrinaBaseDeDatosParsearWhere ("parsear-where-valor-con-espacio%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado = db.parsear_where ("WHERE columna1 = \"texto texto\" AND columna2 = 2 LIMIT 1");

      assert (comparar_arrays (resultado, resultado_esperado));
    });

    Test.add_func ("/parsear-where/valor-con-punto", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var resultado_esperado = new Array<string> ();
      resultado_esperado.append_val ("texto.texto");
      resultado_esperado.append_val ("2");

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new DoctrinaBaseDeDatosParsearWhere ("parsear-where-valor-con-punto-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado = db.parsear_where ("WHERE columna1 = \"texto.texto\" AND columna2 = 2 LIMIT 1");

      assert (comparar_arrays (resultado, resultado_esperado));
    });

    Test.add_func ("/parametrizar-where/ok", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var resultado_esperado = "WHERE columna1 = $1 AND columna2 = $2 LIMIT 1";

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new DoctrinaBaseDeDatosParsearWhere ("parametrizar-where-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado = db.parametrizar_where ("WHERE columna1 = \"texto\" AND columna2 = 2 LIMIT 1");

      assert (resultado == resultado_esperado);
    });

    Test.add_func ("/parametrizar-where/igual-a-campo", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var resultado_esperado = "WHERE columna1 = $1 AND columna2 = tabla.columna LIMIT 1";

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new DoctrinaBaseDeDatosParsearWhere ("parametrizar-where-igual-a-campo-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado = db.parametrizar_where ("WHERE columna1 = \"texto\" AND columna2 = tabla.columna LIMIT 1");

      assert (resultado == resultado_esperado);
    });

    Test.add_func ("/parametrizar-where/valor-con-espacio", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var resultado_esperado = "WHERE columna1 = $1 AND columna2 = tabla.columna LIMIT 1";

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new DoctrinaBaseDeDatosParsearWhere ("parametrizar-where-valor-con-espacio-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado = db.parametrizar_where ("WHERE columna1 = \"texto texto\" AND columna2 = tabla.columna LIMIT 1");

      assert (resultado == resultado_esperado);
    });

    Test.add_func ("/parametrizar-where/valor-con-punto", () => {
      var definicion_db = "CREATE TABLE tabla (columna1 TEXT, columna2 INTEGER); CREATE TABLE db (version INTEGER);";
      var resultado_esperado = "WHERE columna1 = $1 AND columna2 = tabla.columna LIMIT 1";

      var infra = new Doctrina.InfraestructuraMock (definicion_db);
      var db = new DoctrinaBaseDeDatosParsearWhere ("parametrizar-where-valor-con-punto-%s.db".printf (new DateTime.now ().format ("%s")), infra);

      var resultado = db.parametrizar_where ("WHERE columna1 = \"texto.texto\" AND columna2 = tabla.columna LIMIT 1");

      assert (resultado == resultado_esperado);
    });
  }

  public static bool comparar_arrays (Array<string> a, Array<string> b) {
    if (a.length != b.length) {
      debug ("a.length: (%u) != b.length: (%u)".printf (a.length, b.length));
      return false;
    }

    var retorno = true;
    for (uint i = 0; i < a.length; i++) {
      if (a.index (i) != b.index (i)) {
        debug ("index: %u - \"%s\" != \"%s\"".printf (i, a.index (i), b.index (i)));
        retorno = false;
        break;
      }
    }

    return retorno;
  }
}
