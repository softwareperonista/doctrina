/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* doctrina_infraestructura_mock.vala
 *
 * Copyright 2023 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 private class Doctrina.InfraestructuraMock : Object, Doctrina.InfraestructuraInterface {
    private string definicion;

    public InfraestructuraMock (string definicion_db) {
        this.definicion = definicion_db;
    }

    public string directorio_de_configuracion () {
        return Environment.get_tmp_dir();
    }

    public string version_db () {
        return "1";
    }

    public string definicion_db () {
        return this.definicion;
    }

}