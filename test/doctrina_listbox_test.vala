/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* doctrina_listbox_test.vala
 *
 * Copyright 2021 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class DoctrinaListBoxTest {
  public static int main (string[] args) {
    Test.init (ref args);

    if (!Gtk.init_check ()) {
      return 77; // meson exit code to skip test
    }

    DoctrinaListBoxTest.agregar_tests ();

    Test.run ();

    return 0;
  }

  public static void agregar_tests () {
    Test.add_func ("/doctrina/listbox/crear", () => {
      Doctrina.ListBox test_listbox = new Doctrina.ListBox ();

      assert (test_listbox is Doctrina.ListBox);
    });
  }
}