/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* doctrina_row_test.vala
 *
 * Copyright 2021 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class DoctrinaRowTest {
  public static int main (string[] args) {
    Test.init (ref args);

    if (!Gtk.init_check ()) {
      return 77; // meson exit code to skip test
    }

    DoctrinaRowTest.agregar_tests ();

    Test.run ();

    return 0;
  }

  public static void agregar_tests () {
    Test.add_func ("/doctrina/row/crear/label", () => {
      Doctrina.Row test_row = new Doctrina.Row (Doctrina.Row.Tipo.LABEL, "campo_db");

      assert (test_row is Doctrina.Row);
    });

    Test.add_func ("/doctrina/row/crear/entry", () => {
      Doctrina.Row test_row = new Doctrina.Row (Doctrina.Row.Tipo.ENTRY, "campo_db");

      assert (test_row is Doctrina.Row);
    });
    Test.add_func ("/doctrina/row/crear/fino/label", () => {
      Doctrina.Row test_row = new Doctrina.Row.fino (Doctrina.Row.Tipo.LABEL, "campo_db");

      assert (test_row is Doctrina.Row);
    });

    Test.add_func ("/doctrina/row/crear/fino/entry", () => {
      Doctrina.Row test_row = new Doctrina.Row.fino (Doctrina.Row.Tipo.ENTRY, "campo_db");

      assert (test_row is Doctrina.Row);
    });
  }
}