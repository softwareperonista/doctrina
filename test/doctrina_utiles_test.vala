/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* doctrina_base_de_datos_test.vala
 *
 * Copyright 2023 Fernando Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class DoctrinaUtilesTest {
  public static int main (string[] args) {
    Test.init (ref args);

    DoctrinaUtilesTest.agregar_tests ();
    DoctrinaUtilesTest.agregar_join_array_tests ();

    Test.run ();

    return 0;
  }

  public static void agregar_tests () {
    DoctrinaUtilesTest.agregar_separador_de_miles_tests ();
  }

  public static void agregar_separador_de_miles_tests () {
    Test.add_func ("/separador_de_miles/1000", () => {
      var valor = "1000";
      var esperado = "1.000";
      var resultado = Doctrina.Utiles.separador_de_miles (valor);

      assert (resultado == esperado);
    });

    Test.add_func ("/separador_de_miles/123456789", () => {
      var valor = "123456789";
      var esperado = "123.456.789";
      var resultado = Doctrina.Utiles.separador_de_miles (valor);

      assert (resultado == esperado);
    });
  }

  public static void agregar_join_array_tests () {
    Test.add_func ("/join_array/separador-espacio", () => {
      string[] input = { "texto1", "texto2" };
      var separador = " ";
      var esperado = "texto1 texto2";

      var resultado = Doctrina.Utiles.join_array (input, separador);

      assert (resultado == esperado);
    });

    Test.add_func ("/join_array/separador-coma", () => {
      string[] input = { "texto1", "texto2" };
      var separador = ",";
      var esperado = "texto1,texto2";

      var resultado = Doctrina.Utiles.join_array (input, separador);

      if (resultado != esperado) {
        debug ("resultado: " + resultado);
        debug ("esperado: " + esperado);
      }
      assert (resultado == esperado);
    });

    Test.add_func ("/join_array/separador-espacio-caracter-de-cadena", () => {
      string[] input = { "texto1", "texto2" };
      var separador = " ";
      var caracter_de_cadena = "'";
      ;      var esperado = "'texto1' 'texto2'";

      var resultado = Doctrina.Utiles.join_array (input, separador, caracter_de_cadena);

      assert (resultado == esperado);
    });

    Test.add_func ("/join_array/separador-coma-caracter-de-cadena", () => {
      string[] input = { "texto1", "texto2" };
      var separador = ",";
      var caracter_de_cadena = "'";
      var esperado = "'texto1','texto2'";

      var resultado = Doctrina.Utiles.join_array (input, separador, caracter_de_cadena);

      if (resultado != esperado) {
        debug ("resultado: " + resultado);
        debug ("esperado: " + esperado);
      }
      assert (resultado == esperado);
    });
  }
}
