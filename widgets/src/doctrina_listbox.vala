/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* doctrina_listbox.vala
 *
 * Copyright 2018 Andres Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

[GtkTemplate ( ui = "/ar/com/softwareperonista/doctrina/widgets/ui/doctrina_listbox.ui" )]
public class Doctrina.ListBox : Gtk.Grid {
  [GtkChild]
  private unowned Gtk.Label label;
  [GtkChild]
  private unowned Gtk.ListBox listbox;
  [GtkChild]
  private unowned Gtk.ScrolledWindow scrolled_window;
  public int length { get; private set; }
  public int entry_width_chars { get; set; }

  public ListBox ( string label = "" ) {
    this.length = 0;
    this.entry_width_chars = 0;
  }

  public void insert ( Gtk.Widget widget, int posicion ) {
    this.listbox.insert ( widget, posicion );
    this.length++;
  }

  public void insert_row_entry ( string nombre, int posicion, string dato = "" ) {
    var row = new Doctrina.RowEntry ( nombre, dato );

    if ( this.entry_width_chars != 0 ) {
      row.set_width_chars ( this.entry_width_chars );
    }

    this.insert ( row, posicion );
  }

  public void insert_row_label ( string nombre, int posicion, string dato = "" ) {
    var row = new Doctrina.RowLabel ( nombre, dato );
    this.insert ( row, posicion );
  }

  public void insert_lista ( GLib.Array<GLib.Array<string> > datos ) {
    for ( int i = 0; i < datos.length; i++ ) {
      GLib.Array<string> dato = datos.index ( i );
      if ( dato.index ( 0 ) == Doctrina.Row.Tipo.LABEL.to_string () ) {
        Doctrina.RowLabel row = new Doctrina.RowLabel ( dato.index ( 1 ), dato.index ( 2 ) );
        this.insert ( row, -1 );
      } else {
        Doctrina.RowEntry row = new Doctrina.RowEntry ( dato.index ( 1 ), dato.index ( 2 ) );
        this.insert ( row, -1 );
      }
    }
  }

  public Gtk.ListBoxRow get_selected_row () {
    return this.listbox.get_selected_row ();
  }

  public Gtk.ListBoxRow get_row_at_index ( int index ) {
    return this.listbox.get_row_at_index ( index );
  }

  public void limpiar () {
    Gtk.ListBoxRow row = this.listbox.get_row_at_index ( 0 );

    while ( row != null ) {
      row.destroy ();
      row = this.listbox.get_row_at_index ( 0 );
    }

    this.length = 0;
  }

  public void set_label ( string label ) {
    this.label.set_label ( label);
    this.label.set_visible ( true );
  }

  public void set_scrollbar_policy ( Gtk.PolicyType hscrollbar_policy, Gtk.PolicyType vscrollbar_policy ) {
    this.scrolled_window.set_policy ( hscrollbar_policy, vscrollbar_policy );
  }
}
