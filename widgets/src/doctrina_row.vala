/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* doctrina_row.vala
 *
 * Copyright 2018 Andres Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

[GtkTemplate ( ui = "/ar/com/softwareperonista/doctrina/widgets/ui/doctrina_row.ui" )]
public class Doctrina.Row : Gtk.Grid {
  [GtkChild]
  private unowned Gtk.Label label;

  private string campo_db;
  private Row.Tipo tipo;

  public Row ( Row.Tipo tipo, string campo_db ) {
    this.label.set_label ( Doctrina.Utiles.campo_a_nombre( campo_db ) );
    this.tipo = tipo;
    this.campo_db = campo_db;
  }

  public Row.fino ( Row.Tipo tipo, string campo_db ) {
    this.label.set_label ( Doctrina.Utiles.campo_a_nombre( campo_db ) );
    this.tipo = tipo;
    this.campo_db = campo_db;
    this.set_size_request ( 300, 10 );
  }

  public string get_campo_db () {
    return this.campo_db;
  }

  public string get_label_nombre () {
    return this.label.get_label ();
  }

  public void set_campo_db ( string campo_db ) {
    this.campo_db = campo_db;
    this.label.set_label ( Doctrina.Utiles.campo_a_nombre( campo_db ) );
  }

  public enum Tipo {
    LABEL,
    ENTRY;
  }
}
