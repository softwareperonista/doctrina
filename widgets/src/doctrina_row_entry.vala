/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* doctrina_row_entry.vala
 *
 * Copyright 2018 Andres Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

[GtkTemplate ( ui = "/ar/com/softwareperonista/doctrina/widgets/ui/doctrina_row_entry.ui" )]
public class Doctrina.RowEntry : Doctrina.Row {
  [GtkChild]
  private unowned Gtk.Entry entry_dato;

  public RowEntry ( string nombre, string dato_entry = "" ) {
    base ( Tipo.ENTRY, nombre );

    this.entry_dato.set_text ( dato_entry );
  }

  public string get_dato () {
    return this.entry_dato.get_text ();
  }

  public void set_dato ( string dato ) {
    this.entry_dato.set_text ( dato );
  }

  public void set_width_chars ( int width ) {
    this.entry_dato.set_width_chars ( width );
  }
}
