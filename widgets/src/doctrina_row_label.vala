/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* doctrina_row.vala
 *
 * Copyright 2018 Andres Fernandez
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

[GtkTemplate ( ui = "/ar/com/softwareperonista/doctrina/widgets/ui/doctrina_row_label.ui" )]
public class Doctrina.RowLabel : Doctrina.Row {
  [GtkChild]
  private unowned Gtk.Label label_dato;

  public RowLabel ( string nombre, string dato_label = "" ) {
    base ( Tipo.LABEL, nombre );

    this.label_dato.set_label ( dato_label );
  }

  public string get_dato () {
    return this.label_dato.get_label ();
  }

  public void set_dato ( string dato ) {
    this.label_dato.set_label ( dato );
  }
}
